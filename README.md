# Роли для управления серверной инфраструктурой

Предполагается управление серверами на базе CentOS Linux 7.

Описаны следующие роли: ``common``, ``docker-host``, ``etcd-host``, ``docker-swarm-host``, ``pgsql``, ``app-worker``

Пример использования ролей можно найти в файле `hosts/docker-swarm.yaml`. Запустить файл можно командами:

```sh
ansible-playbook hosts/docker-swarm.yaml
```

Пример предполагает наличие группы с именем `docker_swarm` в inventory.

## ``common``

Базовая роль, описывающая стандартную конфигурацию любого сервера. Производит лишь установку дополнительных пакетов:

 - mc
 - vim
 - screen
 - tcpdump
 - bash-completion
 - wget
 - libselinux-python

Так же устанавливается конфигурация репозитория EPEL, включается фаервол и изменяется конфигурация sshd.

## ``docker-host``

Сервера с этой ролью конфигурируются как рабочие машины для запуска docker-контейнеров. Для серверов, к которым будет применяться эта роль, желательно указывать переменную ``docker_thinpool: true`` и ``docker_thinpool_devices: []`` со списком дисков, которые можно использовать для lvm-бэкэнда docker-а.

Устанавливаемое ПО:

 - docker
 - python-docker-py
 - python-requests

Если переменные `docker_thinpool: true` и `docker_thinpool_devices: []` определены, то будет создана группа томов `docker` и thinvolume `data` в этой группе. Будет использовано все свободное пространство дисков из списка `docker_thinpool_devices`

## ``etcd-host``

На серверах, к которым применяется роль `etcd-host`, будет развернут кластер `etcd`. Все сервера будут заведены в этот кластер.

Устанавливаемое ПО:

 -  etcd

## ``docker-swarm-host``

Эта роль включает в себя роли `docker-host` и `etcd-host`. Кроме того, docker настраивается на использование etcd в качестве shared storage, что позволяет использовать overlay-драйвер для сетей docker.

## ``pgsql``

На сервера, выполняющие роль `pgsql`, устанавливается сервер PostgreSQL версии 9.5. Версия может быть указана в переменной `PG_VERSION` в виде 95, или 94, или 93, или 92.

На эти сервера устанавливается официальный репозиторий PostgreSQL, а так же пакеты `posgresql${PG_VERSION}-server` и `postgresql${PG_VERSION}-contrib` 

## ``app-worker``

Роль, которая отвечает за установку всего необходимого ПО на кластер docker-серверов. Для корректного использования этой роли необходимо объявить переменные `appName` и `gitUserName`. Во время конфигурации серверов с этой ролью с bitbucket-а будет загружено приложение `${gitUserName}/${appName}` и развернуто на серверах в docker-контейнерах.

Требования к приложению:

 - приложение должно быть написано с использованием языка python-2
 - приложение может использовать фреймворк Flask
 - приложение может использовать библиотеку psycopg2
 - запускаемый скрипт приложения должен находится в директории bin/${appName}.py
 - во время запуска приложения объявляется переменная `APP_CONFIG`, содержащая путь к конфигурационному файлу приложения в формате json.

## ``dns-server``

Роль для настройки сервера как dns сервера (master или slave, без recursor-а). Используется PowerDNS 4.1
